package sequencer;

import java.util.HashMap;
import java.util.Map;

public class CachingCollatzSequencer extends AbstractCollatzSequencer {
	private Map<Long, Long> map = new HashMap<Long, Long>();
	
	@Override
	public long getLength(long value) {
    	Long length = map.get(value);
   	
    	if (length == null) {
			if (value == 1) {
				length = 1L;
			}
			else {
				length = getLength(next(value)) + 1;
			}
			
        	map.put(value, length);
    	}
		
		return length;
	}
}
