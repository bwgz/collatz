package sequencer;

import org.apache.commons.lang3.tuple.Pair;

public abstract class AbstractCollatzSequencer implements CollatzSequencer {
	protected long next(long value) {
		return (value % 2 == 0) ? value / 2 : value * 3 + 1;
	}
	
	@Override
	public Pair<Long, Long> getPair(long value) {
		return Pair.of(value, getLength(value));
	}
}
