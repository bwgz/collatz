package sequencer;

public class SimpleCollatzSequencer extends AbstractCollatzSequencer {
	@Override
	public long getLength(long value) {
		return (value != 1 ? getLength(next(value)) + 1 : 1);
	}
}
