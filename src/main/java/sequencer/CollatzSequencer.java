package sequencer;

import org.apache.commons.lang3.tuple.Pair;

public interface CollatzSequencer {
	public long getLength(long value);
	public Pair<Long, Long> getPair(long value);
}
