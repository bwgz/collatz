package com.nerdery.euler;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.LongStream;

import org.apache.commons.lang3.tuple.Pair;

/*
 * Collatz Conjecture
 * 
 * Take any natural number n. If n is even, divide it by 2 to get n / 2. If n is odd, multiply it by 3 and add 1 to obtain 3n + 1. 
 * Repeat the process (which has been called "Half Or Triple Plus One", or HOTPO) indefinitely. The conjecture is that no matter 
 * what number you start with, you will always eventually reach 1.
 * 
 * This challenge determines which starting number, under one million, produces the longest chain (sequence).
 */
public class CollatzII {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");

	@FunctionalInterface
	public interface Sequencer {
	  public long length(long value);
	}

	static class PairComparator implements Comparator<Pair<Long, Long>> {
		@Override
		public int compare(Pair<Long, Long> o1, Pair<Long, Long> o2) {
			return (int) (o1.getRight() - o2.getRight());
		}
	}
	
	static class PairConsumer implements Consumer<Pair<Long, Long>> {
		@Override
		public void accept(Pair<Long, Long> pair) {
			System.out.printf("%10d has sequence length %d\n", pair.getLeft(), pair.getRight());
		}
	}
	
	private static final Sequencer sequencer = value -> { return (value == 1) ? 1 : CollatzII.sequencer.length((value % 2 == 0) ? value / 2 : value * 3 + 1) + 1; };
	private static final PairComparator comparator = new PairComparator();
	private static final PairConsumer consumer = new PairConsumer();
	
	private static final List<Pair<Long, Long>> known = Arrays.asList(Pair.of(1L, 1L), Pair.of(6L, 9L), Pair.of(13L, 10L), Pair.of(19L, 21L), Pair.of(27L, 112L), Pair.of(63_728_127L, 950L), Pair.of(670_617_279L, 987L), Pair.of(9_780_657_631L, 1133L));
	private static final long MAX = 1_000_000;

	public static void main(String[] args) {
		
		System.out.println("Calculate the longest chain for a value under " + MAX);
		System.out.println();
		
		System.out.println("Using a simple brute force sequencer ...");
		{
			{
			System.out.println("Some tests producing known results:");
		    known.stream().map(i -> Pair.of(i, sequencer.length(i.getLeft()))).forEach(new Consumer<Pair<Pair<Long, Long>, Long>>() {
				@Override
				public void accept(Pair<Pair<Long, Long>, Long> result) {
					System.out.printf("%10d has sequence length %d (expected: %d)\n", result.getLeft().getLeft(), result.getRight(), result.getLeft().getRight());
				}});
			System.out.println();
			}
		    
		    {
			System.out.println("Calculate answer ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).mapToObj(i -> Pair.of(i, sequencer.length(i))).max(comparator).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
		    }
	        
	        {
			System.out.println("Calculate answer using parallel processing ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).parallel().mapToObj(i -> Pair.of(i, sequencer.length(i))).max(comparator).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
	        }
		}
	}
}
