package com.nerdery.euler;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Internet {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");

	public static void main(String[] args) {

		int number = 1000000;
		 
		int sequenceLength = 0;
		int startingNumber = 0;
		long sequence;
		 
		int[] cache = new int[number + 1];
		//Initialise cache
		for (int i = 0; i < cache.length; i++) {
		    cache[i] = -1;
		}
		cache[1] = 1;
		 
        long start = System.nanoTime();
		for (int i = 2; i <= number; i++) {
		    sequence = i;
		    int k = 0;
		    while (sequence != 1 && sequence >= i) {
		        k++;
		        if ((sequence % 2) == 0) {
		            sequence = sequence / 2;
		        } else {
		            sequence = sequence * 3 + 1;
		        }
		    }
		    //Store result in cache
		    cache[i] = k + cache[(int) sequence];
		 
		    //Check if sequence is the best solution
		    if (cache[i] > sequenceLength) {
		        sequenceLength = cache[i];
		        startingNumber = i;
		    }
		}
		
		System.out.printf("%d %d\n", startingNumber, sequenceLength);
        long end = System.nanoTime();
        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
	}
}
