package com.nerdery.euler;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.function.Consumer;
import java.util.stream.LongStream;

import org.apache.commons.lang3.tuple.Pair;

import sequencer.CachingCollatzSequencer;
import sequencer.CollatzSequencer;
import sequencer.ConcurrentCachingCollatzSequencer;
import sequencer.SimpleCollatzSequencer;

/*
 * Collatz Conjecture
 * 
 * Take any natural number n. If n is even, divide it by 2 to get n / 2. If n is odd, multiply it by 3 and add 1 to obtain 3n + 1. 
 * Repeat the process (which has been called "Half Or Triple Plus One", or HOTPO) indefinitely. The conjecture is that no matter 
 * what number you start with, you will always eventually reach 1.
 * 
 * This challenge determines which starting number, under one million, produces the longest chain (sequence).
 */
public class Collatz {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS");

	static class PairComparator implements Comparator<Pair<Long, Long>> {
		@Override
		public int compare(Pair<Long, Long> o1, Pair<Long, Long> o2) {
			return (int) (o1.getRight() - o2.getRight());
		}
	}
	
	static class PairConsumer implements Consumer<Pair<Long, Long>> {
		@Override
		public void accept(Pair<Long, Long> pair) {
			System.out.printf("%10d has sequence length %d\n", pair.getLeft(), pair.getRight());
		}
	}
	
	private static long[] numbers = new long[] {1, 6, 13, 19, 27, 63728127, 9780657631L};
	private static long MAX = 1_000_000;
	
	private static PairConsumer consumer = new PairConsumer();

	public static void main(String[] args) {
		
		System.out.println("Calculate the longest chain for a value under " + MAX);
		System.out.println();
		
		{
			CollatzSequencer sequencer = new SimpleCollatzSequencer();
			System.out.println("Using a simple brute force sequencer ...");
			System.out.println("Some tests producing known results");
		    Arrays.stream(numbers).mapToObj(i -> sequencer.getPair(i)).forEach(consumer);
			System.out.println();
		    
		    {
			System.out.println("Calculate answer ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).mapToObj(i -> sequencer.getPair(i)).max(new PairComparator()).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
		    }
	        
	        {
			System.out.println("Calculate answer in parallel ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).parallel().mapToObj(i -> sequencer.getPair(i)).max(new PairComparator()).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
	        }
		}
		
		{
			{
			CollatzSequencer sequencer = new CachingCollatzSequencer();
			System.out.println("Using a sequencer that caches the intermediate values ...");
			System.out.println("Some tests producing known results");
		    Arrays.stream(numbers).mapToObj(i -> sequencer.getPair(i)).forEach(consumer);
			System.out.println();
			}
		    
		    {
		    CollatzSequencer sequencer = new CachingCollatzSequencer();
			System.out.println("Calculate answer ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).mapToObj(i -> sequencer.getPair(i)).max(new PairComparator()).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
		    }
	        
	        {
			CollatzSequencer sequencer = new ConcurrentCachingCollatzSequencer();
			System.out.println("Calculate answer in parallel ...");
	        long start = System.nanoTime();
			LongStream.range(1, MAX).parallel().mapToObj(i -> sequencer.getPair(i)).max(new PairComparator()).ifPresent(consumer); 
	        long end = System.nanoTime();
	        System.out.println("execution time: " + sdf.format(new Date((end - start) / 1_000_000)));
			System.out.println();
	        }
		}
	}
}
